# distributed-gallery 2016 version

For displaying assets from Brown Digital Repository on a digital sign, and allows user to view additional information about the sign on their mobile device. Also allows for the viewer to control what is displayed on the sign.

Sign: /sign.html
Mobile: /index.html

